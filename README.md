CSCI-652 Distributed Systems
For more info, please visit my [teaching](https://www.cs.rit.edu/~ph/teaching) page.

For RIT students, you can find more information on myCourses.

Run ```mvn package``` inside the folder of each module to compile and generate a .jar file.


If you need to sync your forked repo with upstream repo, check [here](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/syncing-a-fork)
