public enum SubscriberTag {
    INIT(0), PULL_TOPIC(1), SUBSCRIBE(2), LOGIN(3), READY_TO_RECV(4), // not used
    ALIVE(5), // todo
    OFFLINE(6),
    PREV(7);//get past messages

    public final int id;

    SubscriberTag(int i) {
        this.id = i;
    }
}