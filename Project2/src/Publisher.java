import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Publisher {
    static final int SERVER_PORT = 40000;
    static Connection serverConnection = null;
    static Scanner inputReader = new Scanner(System.in);

    // filter and map user input to command tag
    static int readUserInput() {
        String str = inputReader.nextLine();
        return 1;
    }

    // static void logout() throws Exception {
    //     serverConnection.send(Message.generatePubHeader(PublisherTag.LOGOUT) + Message.MESSAGE_END);
    //     serverConnection.close();
    // }

    static void getTopics() {
        serverConnection.send(Message.generatePubHeader(PublisherTag.PULL_TOPIC) + Message.MESSAGE_END);
    }

    static void publishTopics(String topicName) {
        serverConnection.send(Message.generatePubHeader(PublisherTag.TOPIC) + topicName + Message.MESSAGE_END);
    }

    static void publishContent(int topicID, String content){
        serverConnection.send(Message.generatePubHeader(PublisherTag.PUB) + topicID + '\n'
         + content + Message.MESSAGE_END);
    }

    static void connectionInit(String serverHostname) throws UnknownHostException, IOException {
        serverConnection = new Connection(new Socket(serverHostname, SERVER_PORT), (in) -> {
            try {
                String str = "";
                while(in.ready()){
                    str = in.readLine();
                    System.out.println(str);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        serverConnection.start();

        //when it started it will try to contect the manager
        System.err.println("started");
        serverConnection.send(Message.generatePubHeader(PublisherTag.LOGIN) + Message.MESSAGE_END);
    }

    public static void main(String[] args) throws Exception {
        // default host name
        String serverHostname = new String("127.0.0.1");
        // take input
        if (args.length > 0)
            serverHostname = args[0];

        System.out.println("Attemping to connect to host " + serverHostname + " on port 40000.");

        // set up connection
        try {
            connectionInit(serverHostname);
            Thread.sleep(2000);
            test();

            // re-login test
            // Thread.sleep(1000);
            // connectionInit(serverHostname);
            // Thread.sleep(1000);
            // logout();
        } catch (IOException e){
            e.printStackTrace();
        } finally{
            serverConnection.close();
        }
    }

    static void test() throws Exception {
        //pull topics
        System.err.println("pull topics request");
        getTopics();
        publishTopics("a truthful news");
        publishContent(2, "The great Napoleon is about to arrive in his faithful Paris");
        Thread.sleep(1000);
        // logout();
    }
}