import java.net.Socket;
import java.util.function.Consumer;
import java.util.function.Function;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.ServerSocket;

public class Server extends Thread {
    static final int port = 40000;
    boolean terminateFlag = true;
    Function<Integer, MsgProcessor> subGenerator = null;
    int connectionNum = 0;

    // test connection
    class ConnectionStatus {
        public Connection connection = null;
        public boolean busy = false;
        public void setUpConnection(Socket socket, Consumer<BufferedReader> consumer){
            connection = new Connection(socket, consumer);
            busy = true;
            connection.start();
        }
    }

    ConnectionStatus connections[] = new ConnectionStatus[10000];

    public Server(Function<Integer, MsgProcessor> subGenerator) {
        this.subGenerator = subGenerator;
    }

    public void send(int id, Object message) {
        connections[id].connection.send(message);
    }

    public void closeConnection(int id) {
        try {
            connections[id].connection.close();
        } catch (Exception e) {
            System.err.println("error closing the socket");
            e.printStackTrace();
        }
        connections[id].busy = false;
    }

    //todo: check for the connection slot is busy or not
    private int nextNum(){
        if(connectionNum < 10000) return connectionNum++;
        else return (connectionNum = 0);
    }

    public void run() {
        // create socket
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.err.println("Started server on port " + port);
                    // repeatedly wait for connections, and process
            while (terminateFlag) {

                // a "blocking" call which waits until a connection is requested
                Socket clientSocket = serverSocket.accept();
                System.err.println("Accepted connection from client");
                int num = nextNum();
                connections[num] = new ConnectionStatus();
                connections[num].setUpConnection(clientSocket, subGenerator.apply(num));
            }
            serverSocket.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}