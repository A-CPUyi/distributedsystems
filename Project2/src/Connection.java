import java.io.*;
import java.net.*;
import java.util.function.Consumer;

/**
 * Connection
 */
public class Connection extends Thread {
    BufferedWriter out = null;
    BufferedReader in = null;
    Socket socket = null;
    Consumer<BufferedReader> consumer = null;

    // set up a connection with an accepted socket
    public Connection(Socket socket, Consumer<BufferedReader> consumer) {
        this.socket = socket;
        this.consumer = consumer;
        try {
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("unable to set up connetion\n");
        }
    }

    @Override
    public void run() {
        while (true) {
            if (socket.isClosed()) {
                System.out.println("connection gone");
                break;
            }

            try {
                if (in.ready()) {
                    consumer.accept(in);
                } else {
                    sleep(10);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.err.println("unexpected thread close");
            }
        }
    }

    public void send(Object obj) {
        try {
            out.write(obj.toString());
            out.flush();
        } catch (IOException e) {
            System.err.println("the connection is gone");
            this.interrupt();
            e.printStackTrace();
        }
    }

    public void close() throws Exception{
        out.close();
        in.close();
        socket.close();
    }
}