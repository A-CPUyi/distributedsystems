import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 * subscriber has following Features: 1. make first contact with the manager 2.
 * pull topic list 3. request for subscribe 4. report to server when comes
 * online 5. receive contents todo: 6. reply to heartbeat connection check 7. go
 * offline
 */
public class Subscriber {
    static final int SERVER_PORT = 40000;
    static Connection serverConnection = null;
    static Scanner inputReader = new Scanner(System.in);

    // by default subscriber id = 0
    static private int sub_id = 0;

    static private String createHeader(SubscriberTag tag) {
        return "sub:" + tag.id + '\n';
    }

    // filter and map user input to command tag
    static int readUserInput() {
        String str = inputReader.nextLine();
        return 1;
    }

    static void logout() throws Exception {
        serverConnection.send(createHeader(SubscriberTag.OFFLINE) + Message.MESSAGE_END);
        serverConnection.close();
    }

    static void getTopics() {
        serverConnection.send(createHeader(SubscriberTag.PULL_TOPIC) + Message.MESSAGE_END);
    }

    static void subscribeTopics(int[] topicTags) {
        String message = "";
        for (int i : topicTags) {
            message += "" + i + '\n';
        }
        serverConnection.send(createHeader(SubscriberTag.SUBSCRIBE) + message + Message.MESSAGE_END);
    }

    static void connectionInit(String serverHostname) throws UnknownHostException, IOException {
        serverConnection = new Connection(new Socket(serverHostname, SERVER_PORT), (in) -> {
            try {
                String str = "";
                while(in.ready()){
                    str = in.readLine();
                    if(str.matches("sub:\\d")){
                        int tag = Integer.parseInt(str, 4, str.length(), 10);
                        // System.out.println("receved reply: " + tag);
                        if(tag == SubscriberTag.INIT.id){
                            sub_id = Integer.parseInt(in.readLine());
                        }
                    } else {
                        System.out.println(str);
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        serverConnection.start();

        //when it started it will try to contect the manager
        System.err.println("started");
        if(sub_id != 0){
            serverConnection.send(createHeader(SubscriberTag.LOGIN) + sub_id + Message.MESSAGE_END);
        } else {
            serverConnection.send(createHeader(SubscriberTag.INIT) + Message.MESSAGE_END);
        }
    }

    public static void main(String[] args) throws Exception {
        // default host name
        String serverHostname = new String("127.0.0.1");
        // take input
        if (args.length > 0)
            serverHostname = args[0];

        System.out.println("Attemping to connect to host " + serverHostname + " on port 40000.");

        // set up connection
        try {
            connectionInit(serverHostname);
            Thread.sleep(2000);
            test();

            // re-login test
            // Thread.sleep(1000);
            // connectionInit(serverHostname);
            // Thread.sleep(1000);
            // logout();
        } catch (IOException e){
            e.printStackTrace();
        } finally{
            serverConnection.close();
        }
    }

    static void test() throws Exception {
        //pull topics
        System.err.println("pull topics request");
        getTopics();

        //subscribe
        int[] testTopics = new  int[]{0,1,3,4};
        subscribeTopics(testTopics);
        Thread.sleep(1000);
        logout();
    }
}