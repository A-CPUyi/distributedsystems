public enum PublisherTag{
    LOGIN(0),TOPIC(1),PUB(2),PULL_TOPIC(3);
    public final int id;

    PublisherTag(int i) {
        this.id = i;
    }
}