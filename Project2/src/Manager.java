import java.util.HashMap;
import java.util.Vector;
import java.util.function.Function;

public class Manager {

    private class Content{
        public String topicName;
        public int topicID;
        public String content;

        public String toString(){
            String message = "" + topicID + ' ' + topicName + '\n'
             + content;
            return message;
        }
    }

    private class Topic {
        public Topic(String name, int topic_id) {
            this.name = name;
            this.topic_id = topic_id;
        }
        public String name;
        public int topic_id;
        public Vector<Content> contents = new Vector<>();
        public Vector<Integer> subscriberID = new Vector<>();

        //subscribe this topic
        public void subscribe(int id){
            if(subscriberID.contains(id)) subscriberID.remove(id);
            else {
                subscriberID.add(id);
            }
        }

        //when a topic receives a new content
        public void push(String str){
            Content temp = new Content();
            temp.topicName = this.name;
            temp.topicID = this.topic_id;
            temp.content = str;
            contents.add(temp);
            // add to subscriber's queue
            for(int sub_id : subscriberID){
                // System.out.println("subscribed by: " + sub_id);
                Subscriber subscriber = subscriberInfo.get(sub_id);
                if(subscriber.processor != null){
                    subscriber.processor.send(temp.toString());
                } else {
                    subscriber.pendingMessage.add(temp);
                }
            }
        }
    }

    class Subscriber{
        public Subscriber(int id){this.id = id;}
        public int id;
        public Vector<Integer> subscribedList = new Vector<>();
        public Vector<Content> pendingMessage = new Vector<>();
        public MsgProcessor processor = null;
    }

    // to ensure thread safe
    private class IDCounter {
        int currentID[] = new int[] { 1 };

        public int getNextID() {
            int id;
            synchronized (currentID) {
                id = currentID[0]++;
            }
            return id;
        }
    }

    private Server server;
    private IDCounter idCounter = new IDCounter();
    private HashMap<Integer, Subscriber> subscriberInfo = new HashMap<>();
    private Vector<Topic> topicList = new Vector<>();

    //common
    public String[] getAllTopics(){
        String topics[] = new String[topicList.size()];
        for(int i=0;i<topicList.size();i++){
            topics[i] = "" + i + ' ' + topicList.get(i).name;
        }
        return topics;
    }

    public void closeConnection(int id){
        server.closeConnection(id);
    }

    //subscriber related
    Function<Integer,MsgProcessor> subProcessorGenerater = (connectionID) -> {
        return new MsgProcessor(this, connectionID);
    };

    public int getNextID(){
        return idCounter.getNextID();
    }

    public void send(int subscriberID, Object obj){
        server.send(subscriberID, obj);
    }

    public void addID(int id){
        subscriberInfo.computeIfAbsent(id, (i)->new Subscriber(id));
    }

    public void subscribeTopic(int sub_id, Vector<Integer> tags){
        Subscriber temp = subscriberInfo.get(sub_id);
        for(int i : tags){
            if(temp.subscribedList.contains(i)) 
                //if call remove with int it will remove the item at the index
                temp.subscribedList.remove(temp.subscribedList.indexOf(i));
            else temp.subscribedList.add(i);
            if(i < topicList.size()){
                Topic topic = topicList.get(i);
                topic.subscribe(sub_id);
            }
        }
    }

    //publisher related
    public boolean addTopic(String topicName){
        topicList.add(new Topic(topicName, topicList.size()));
        return true;
    }
    
    public void push(int topicID, String content){
        topicList.get(topicID).push(content);
    }

    public void subscriberConnection(int sub_id, MsgProcessor processor){
        Subscriber subscriber = subscriberInfo.get(sub_id);
        subscriber.processor = processor;
        while(subscriber.pendingMessage.size() > 0){
            subscriber.processor.send(subscriber.pendingMessage.get(0).toString());
            subscriber.pendingMessage.remove(0);
        }
    }

    public boolean isSubscriber(int id){
        return subscriberInfo.containsKey(id);
    }

    public static void main(String[] args) throws InterruptedException {
        Manager test = new Manager();
        test.addTopic("shitty news");
        test.addTopic("mother fucker");

        test.server = new Server(test.subProcessorGenerater);
        test.server.start();
        Thread.sleep(5000);
        System.out.println("try push sth");
        test.topicList.get(0).push("newly made up soviet jokes\n");
    }
}