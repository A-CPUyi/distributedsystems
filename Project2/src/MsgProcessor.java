import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;
import java.util.function.Consumer;

public class MsgProcessor implements Consumer<BufferedReader> {
    int subscriberID = 0;
    int connectionID= 0;
    Manager manager;
    public MsgProcessor(Manager manager, Integer connectionID){
        this.manager = manager;
        this.connectionID = connectionID;
    }

    private int processSubInit(){
        int sub_id = manager.getNextID();
        manager.send(connectionID, Message.generateSubHeader(SubscriberTag.INIT)
         + sub_id + Message.MESSAGE_END);
        manager.addID(sub_id);
        manager.subscriberConnection(sub_id, this);
        return sub_id;
    }

    private void processSubscribe(int id, Vector<Integer> tags){
        if(tags.size() > 0){
            manager.subscribeTopic(id, tags);
        }
    }

    private void processPullTopics(){
        System.out.println("requested for topic list");
        String topics[] = manager.getAllTopics();
        String message = "";
        for (String str : topics) {
            message += str + "\n";
        }
        manager.send(connectionID, message);
    }

    public void send(Object obj){
        manager.send(connectionID, obj);
    }

    @Override
    public void accept(BufferedReader inStream) {
        String str = "";
        try {
            while (inStream.ready()) {
                str = inStream.readLine();
                //if it is header
                if(str.matches("sub:\\d")){
                    int tag = Integer.parseInt(str, 4, str.length(), 10);
                    System.out.println("receved command: " + tag);
                    str = inStream.readLine();
                    //why i can't use switch case here?
                    if(tag == SubscriberTag.INIT.id){
                        subscriberID = processSubInit();
                    } else if(tag == SubscriberTag.LOGIN.id){
                        subscriberID = Integer.parseInt(str);
                        //if id exsist, send over message(topic contents)
                        if(manager.isSubscriber(subscriberID)){
                            System.out.println("valid: " + subscriberID);
                            manager.subscriberConnection(subscriberID, this);
                        }
                    } else if(tag == SubscriberTag.OFFLINE.id){
                        manager.closeConnection(connectionID);
                    } else if(tag == SubscriberTag.PULL_TOPIC.id){
                        processPullTopics();
                    } else if(tag == SubscriberTag.SUBSCRIBE.id){
                        Vector<Integer> tags = new Vector<>();
                        while(str.compareTo(Message.MESSAGE_END.strip())!=0){
                            if(str.length() > 0)
                                tags.add(Integer.parseInt(str));
                            str = inStream.readLine();
                        }
                        processSubscribe(subscriberID, tags);
                    }
                }
                else if(str.matches("pub:\\d")){
                    int tag = Integer.parseInt(str, 4, str.length(), 10);
                    System.out.println("receved command: " + tag);
                    str = inStream.readLine();
                    if(tag == PublisherTag.LOGIN.id){
                        //do nothing
                    } else if(tag == PublisherTag.PUB.id){
                        int topicTag = Integer.parseInt(str);
                        String message = "";
                        str = inStream.readLine();
                        while(str.compareTo(Message.MESSAGE_END.strip())!=0){
                            message += str + '\n'; 
                        }
                        manager.push(topicTag, message);
                    } else if(tag == PublisherTag.TOPIC.id){
                        manager.addTopic(str);
                    } else if(tag == PublisherTag.PULL_TOPIC.id){
                        processPullTopics();
                    }
                }
            }
        } catch (IOException e) {
            //TODO: handle exception
        } catch (NumberFormatException e2){
            e2.printStackTrace();
        }
    }
}