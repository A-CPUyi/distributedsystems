/**
 * Message
 */
public class Message {
    public static final String MESSAGE_END = "\nENDEND\n";
    public static String generateSubHeader(SubscriberTag t){
        return "sub:" + t.id + '\n';
    }
    public static String generatePubHeader(PublisherTag t){
        return "sub:" + t.id + '\n';
    }
}