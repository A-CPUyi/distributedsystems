import java.io.IOException;
import java.util.function.Consumer;

public abstract class DataProcessor implements Consumer<String>
{

    //process the received data
    public abstract void process() throws IOException;
}