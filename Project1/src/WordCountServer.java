import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class WordCountServer {
    final static int port = 8000;
    static ServerSocket serverSocket;

    public static void main(String[] args) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (Exception e) {
            System.out.println("unable to locate server socket");
        }
        System.out.println("TCP Server is running and accepting client connections...");
        try {
            Socket connectionSocket = serverSocket.accept();
            new Connection(connectionSocket, new WordCountProcessor());
            Thread.sleep(10000);
        } catch (IOException | InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

class Connection extends Thread {
    static final String endSign = "finalfinalfinal";
    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;
    DataProcessor processor; 

    public Connection(Socket aClientSocket, DataProcessor processor) {
        try {
            clientSocket = aClientSocket;
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
            this.processor = processor;
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public void run() {
        try { 
            //loop until sth in the stream
            while(in.available() == 0){}
            String buffer = "";
            while(buffer.compareTo(endSign) != 0){
                buffer = in.readUTF();
                // System.out.println("Received :" + buffer);
                if(buffer.compareTo(endSign) != 0) processor.accept(buffer);
            }
            // System.out.println("i'm here");
            //after receving end sign
            processor.process();
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        }
    }
}

class WordCountProcessor extends DataProcessor {
    String filePath = "../recvFile/testData.csv";
    BufferedWriter writer;

    WordCountProcessor() throws IOException{
       writer = new BufferedWriter(new FileWriter(filePath, true));
       writer.write("");
    }

    @Override
    //with this method there is a significant drawback that if the file is priviously
    //written with sth, it will append to the end of the file rather overwirte it.
    public void accept(String t) {
        //write to a file
        try {
            writer.append(t);
            writer.newLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void process() throws IOException{
        writer.close();
        // TODO Auto-generated method stub
        List<AmazonFineFoodReview> allReviews = WordCount_Seq_Improved.read_reviews(filePath);
        List<KV<String, Integer>> kv_pairs = WordCount_Seq_Improved.map(allReviews);
        Map<String, Integer> results = WordCount_Seq_Improved.reduce(kv_pairs);
        WordCount_Seq_Improved.print_word_count(results);
    }
}