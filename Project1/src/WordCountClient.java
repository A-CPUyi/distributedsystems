import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;

public class WordCountClient {
    static final int serverPortNum = 8000;
    static final String endSign = "finalfinalfinal";
    static Socket clientSocket;

    public static void main(String[] args) {
        DataInputStream dataIn = null;
        DataOutputStream dataOut = null;
        try {
            clientSocket = new Socket(args[0], serverPortNum);
            dataIn = new DataInputStream(clientSocket.getInputStream());
            dataOut = new DataOutputStream(clientSocket.getOutputStream());
            if(dataOut != null) System.out.println("data out not null");
        } catch (Exception e) {
            System.err.println("unable to connect to a server");
        }
        System.out.println("connected to the server");
        BufferedReader readIn = null;
        try {
            readIn = readInCSV(args[1]);
        } catch (FileNotFoundException fnf) {
            System.err.println(fnf.getMessage());
        } finally {
            if (readIn == null) {
                System.err.println("unable to read in csv file");
                return;
            }
        }
        System.out.println("try send the csv file");
        try {
            while (readIn.ready()) {
                String str = readIn.readLine();
                dataOut.writeUTF(str);
                // System.out.println("read: " + str);
            }

            //finish up transmition
            dataOut.writeUTF(endSign);
            clientSocket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    static BufferedReader readInCSV(String filePath) throws FileNotFoundException{
        return new BufferedReader(new FileReader(filePath));
    }
}